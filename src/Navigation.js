import React from 'react';
import { Navigation } from '@shopify/polaris';
import { CollectionsMajor, CustomersMajor } from '@shopify/polaris-icons';
import {withRouter} from "react-router-dom";

class Navigationn extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
    }
    nextPath = (url) => {  
        this.props.history.push(url);
    };

    render() {
        return (
            <Navigation location="/Customers">
                <Navigation.Section
                    items={[
                        {
                            label: 'Customers',
                            icon: CustomersMajor,
                            onClick: () => this.nextPath('/Customers'),
                        },
                        {
                            label: 'Countries',
                            icon: CollectionsMajor,
                            onClick: () => this.nextPath('/Countries'),
                        },
                    ]}
                />
            </Navigation>
        );
    }
}
export default withRouter(Navigationn);
