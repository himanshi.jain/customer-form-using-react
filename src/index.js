import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import en from '@shopify/polaris/locales/en.json';
import { AppProvider, Frame } from "@shopify/polaris";

import Navigation from "./Navigation"
import Customers from "./Customers";
import Countries from "./Countries";
import TopBar from "./TopBar";

const theme = {
    logo: {
        width: 124,
        topBarSource:
            'https://cdn.shopify.com/s/files/1/0446/6937/files/jaded-pixel-logo-color.svg?6215648040070010999',
        url: 'http://jadedpixel.com',
        accessibilityLabel: 'Traveling across countries',
    },
};

ReactDOM.render(
    <AppProvider
        theme={theme}
        i18n={en} >
        <Router>
            <Frame topBar={<TopBar />} navigation={<Navigation />}>
                <Switch>
                    <Route path="/Customers">
                        <Customers />
                    </Route>
                    <Route path="/Countries">
                        <Countries />
                    </Route>
                </Switch>
            </Frame>
        </Router>
    </AppProvider>,
    document.getElementById("root")
);