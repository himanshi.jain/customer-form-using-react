import React from "react";
import { Card, DataTable, Page } from "@shopify/polaris";

export default function Countries() {
    // fetch(`https://restcountries.eu/rest/v2/all`)
    //     .then(res => res.json())
    //     .then(data => Countries(data))
    //     .catch(err => console.log("Error : ", err))

    // let rows = [[data.region, data.name, data.subregion, data.capital, data.callingCodes[0], data.population]];
    let rows = [['Americas', 'United States of America', 'Northern America', 'Washington, D.C.', 1, 323947000]];

    return (
        <Page title="World Countries List">
            <Card>
                <DataTable
                    columnContentTypes={["text", "text", "text", "text", "numeric", "numeric"]}
                    headings={["Continent", "Country", "Subregion", "Capital City", "Calling Code", "Population"]}
                    rows={rows}
                    hoverable={true}
                />
            </Card>
        </Page>
    );

}