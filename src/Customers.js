import React from 'react';
import { Button, TextContainer, Modal, TextField, Form, FormLayout, Toast, Page, Card, DataTable } from '@shopify/polaris';
import validator from 'validator';

export default class ModalExample extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname: "",
            email: "",
            tel: "",
            active: false,
            toast: false,
            errors: {
                fullname:"",
                email:"",
                tel:""
            }
        }
    }
    validation = () => {
        let isError = false;
        let errors = this.state.errors;
        let { fullname, email, tel } = this.state;
        if (fullname === null || fullname === '') {
            errors = { ...errors, fullname: 'Name is required' };
            isError = true;
        } else if(/\d/.test(fullname)){
            errors = { ...errors, fullname: 'Only alphabets are allowed' };
            isError = true;
        }
        else {
            errors = { ...errors, fullname: '' };
        }

        if (email === null || email === '') {
            errors = { ...errors, email: 'Email is required' };
            isError = true;
        }else if (!validator.isEmail(email)) {
            errors = { ...errors, email: 'Not a valid email' };
            isError = true;          }
         else {
            errors = { ...errors, email: '' };
        }

        if (tel === null || tel === '') {
            errors = { ...errors, tel: 'Phone number is required' };
            isError = true;
        }else if(/[a-z]/i.test(tel)){
            errors = { ...errors, tel: 'Only numbers are allowed' };
            isError = true;
        }else if(tel.length!==10){
            errors = { ...errors, tel: 'Phone number must be 10 digits' };
            isError = true;
        }
         else {
            errors = { ...errors, tel: '' };
        }
        this.setState({ errors: errors });
        return isError;
    }
    showOutputOnTable() {
        var records = [];
        let rows = []; 
        if (localStorage.length !== 0) {
            records = JSON.parse(localStorage.getItem("users"));
        } else {
            records = [];
        }
        if (records) {
            records.map(element => {
                rows.push([
                    element.FullName,
                    element.Email,
                    element.PhoneNumber
                ])
            });
        }
        return rows;
    }
    onClickButton = () => {
        this.setState({ active: true });
        this.setState({ fullname: "", email: "", tel: "" });
    }
    onCloseModal = () => {
        this.setState({ active: false });
        this.setState({ errors: {
            fullname:"",
            email:"",
            tel:""
        }});
    }
    handleSubmit = (e) => {
        if (!this.validation()) {
            var records = [];
            records = JSON.parse(localStorage.getItem("users")) ? JSON.parse(localStorage.getItem("users")) : []
            records.push({
                "FullName": this.state.fullname,
                "Email": this.state.email,
                "PhoneNumber": this.state.tel,
            });
            localStorage.setItem("users", JSON.stringify(records));

            this.setState({ active: false, toast: true });
            this.setState({ fullname: "", email: "", tel: "" });
        }
    }
    toggleActive = () => this.setState({ toast: false });

    render() {
        var rows = this.showOutputOnTable();
        const toastMarkup = this.state.toast ? (
            <Toast content="Registration successful" onDismiss={this.toggleActive} />
        ) : null;
        return (
            <Page primaryAction={{ content: 'Register', onAction: this.onClickButton}} title="Registered Customers List">
                <Card>
                    <DataTable
                        columnContentTypes={["text", "text", "numeric"]}
                        headings={["Name", "Email", "Phone Number"]}
                        rows={rows}
                        verticalAlign="middle"
                        showTotalsInFooter={false}
                    />
                </Card>
                <div style={{ height: '500px' }}>
                    <Modal
                        open={this.state.active}
                        onClose={this.onCloseModal}
                        title="Registration Form"
                    >
                        <Modal.Section>
                            <TextContainer>
                                <Form onSubmit={this.handleSubmit} implicitSubmit={true} autoComplete={true} noValidate={true}>
                                    <FormLayout>
                                        <TextField
                                            value={this.state.fullname}
                                            onChange={(value) => this.setState({ fullname: value })}
                                            label="Full Name"
                                            type="text"
                                            name="fullname"
                                            requiredIndicator={true}
                                            error={this.state.errors.fullname}
                                        />
                                        <TextField
                                            value={this.state.email}
                                            onChange={(value) => this.setState({ email: value })}
                                            label="Email"
                                            type="email"
                                            name="email"
                                            requiredIndicator={true}
                                            error={this.state.errors.email}
                                        />
                                        <TextField
                                            value={this.state.tel}
                                            onChange={(value) => this.setState({ tel: value })}
                                            label="Phone Number"
                                            type="tel"
                                            name="tel"
                                            requiredIndicator={true}
                                            maxLength={10}
                                            error={this.state.errors.tel}
                                        />
                                        <Button submit>Submit</Button>
                                    </FormLayout>
                                </Form>
                            </TextContainer>
                        </Modal.Section>
                    </Modal>
                    {toastMarkup}
                </div>
            </Page>

        );
    }
}